#!/bin/bash
echo "Mounting share/bilder"
mkdir -p ./bilder
sshfs -o reconnect -o nonempty -o compression=yes morph:/home/share/bilder/ bilder/
echo "Mounting christoph/Archiv"
mkdir -p ./archiv
sshfs -o reconnect -o nonempty -o compression=yes morph:/home/christoph/Archiv/ archiv/

