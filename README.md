# Christoph's dotfiles
After moving all systems to Linux/Unix, I start my own dotfiles repo to keep every machine in sync and up-to-date.

## Get started

short version for Ubuntu systems

```bash
wget -O - chr1shaefn3r.me/setup.sh | bash
```

or long version:

```sh
curl --proto '=https' --tlsv1.2 -sSf https://gitlab.com/chr1shaefn3r/dotfiles/-/raw/master/setup.sh | sh
```

## Inspiration
After reading through http://dotfiles.github.io and many of the linked projects I ended up at https://github.com/mathiasbynens/dotfiles.
So this is mostly a personalized version of his work plus some additionally features.

## Features
### Zero Prerequisites
What was important to me is, that I have a setup-script which starts with a naked "Linux/Unix", instead of having implicit prerequisites like git.
So I added a setup.sh-script which starts by installing all my favourite packages and then sets up all the dotfiles.

### Host specific settings
I still wanted to be able to have specific setups for every machine in use.
The current setup is to use branches, named after the hostnames they are used on, to create host-optimized setups.

### Automatic update
Additionally I added an automatic update feature to .bashrc.
Now every change I make is automatically incorporated to all the cloned instances (pull-based on every new terminal startup).
