#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install Spotify"

m_header "Add signing key"
wget -O- https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -

m_header "Add Spotify repository"
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list

m_header "Update list of available packages"
sudo apt-get update

m_header "Install Spotify"
sudo apt-get install -y spotify-client

m_success "Successfully installed Spotify"

