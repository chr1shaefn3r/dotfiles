#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install .heic Environment"

sudo apt-get install -y libheif-examples
m_success "Installed heif-convert"

m_success "Successfully installed .heic Environment"

#for file in *.heic; do heif-convert $file ${file/%.heic/.jpg}; done

