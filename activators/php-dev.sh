#!/usr/bin/env bash

if [ -d ~/Development/web/ ]; then
	exit 0;
fi

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

mkdir -p ~/Development/web;
m_success "Created ~/Development/web"

sudo apt-get install -y php-cli

m_success "Successfully installed Web Development Environment"
