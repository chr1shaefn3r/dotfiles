#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install python development Environment"

sudo apt-get install python3-pip
m_success "Installed pip"

sudo pip install --system pipenv
m_success "Installed pipenv"

sudo apt-get install -y build-essential cmake
m_success "Installed build-essential and cmake for python debs that need to be compiled"

m_success "Successfully installed python development Environment"

