#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install gnome-shell-extensions"

sudo apt-get install gnome-shell-extensions chrome-gnome-shell
m_success "Installed shell-extensions and native host connector"

firefox https://extensions.gnome.org/extension/906/sound-output-device-chooser/ &
firefox https://extensions.gnome.org/extension/750/openweather/ &


m_success "Successfully installed gnome-shell-extensions"

