#!/bin/bash

DIR="./**/";
for f in $DIR*.mkv
do
	echo "Processing file: '$f' ..."
	BASE=`basename "$f"`
	DIRNAME=`dirname "$f"`
	FILENAME="${BASE%.*}"
	OUTPUT="$DIRNAME/$FILENAME.h265.mkv";
	ffmpeg -i "$f" -map 0 -c copy -preset slower -vaapi_device /dev/dri/by-path/pci-0000:08:00.0-render -c:v:0 hevc_vaapi -filter_complex '[0:v] format=nv12,hwupload' -bf 0 -max_muxing_queue_size 4096 "$OUTPUT"
done

