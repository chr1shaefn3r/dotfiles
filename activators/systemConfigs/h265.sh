#!/bin/bash

DIR="./**/";
for f in $DIR*.mkv
do
	echo "Processing file: '$f' ..."
	BASE=`basename "$f"`
	DIRNAME=`dirname "$f"`
	FILENAME="${BASE%.*}"
	OUTPUT="$DIRNAME/$FILENAME.h265.mkv";
	echo $OUTPUT
	if [ -e "$OUTPUT" ]; then
		continue;
	fi;
	ffmpeg -i "$f" -map 0 -c copy -vcodec hevc -preset slower -threads 8 "$OUTPUT"
	if [ $? -ne 0 ]; then
		break;
	fi;
done

