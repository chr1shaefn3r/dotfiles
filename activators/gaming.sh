#!/usr/bin/env bash

set -e

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install Gaming Environment"

m_header "Install TS3"
cd /opt/
TS_VERSION=3.5.2
sudo wget http://dl.4players.de/ts/releases/$TS_VERSION/TeamSpeak3-Client-linux_amd64-$TS_VERSION.run
sudo chmod +x TeamSpeak3-Client-linux_amd64-$TS_VERSION.run
sudo ./TeamSpeak3-Client-linux_amd64-$TS_VERSION.run
sudo mv TeamSpeak3-Client-linux_amd64/ ts3/
sudo chown -R christoph:christoph ts3/
sudo chmod -R 775 ts3/
sudo rm TeamSpeak3-Client-linux_amd64-$TS_VERSION.run
cd ~/bin/
ln -s /opt/ts3/ts3client_runscript.sh ts3
m_success "Installed TS3 Client"

m_header "Install Steam"
sudo apt-get install steam
m_success "Installed Steam client"

m_success "Successfully installed Gaming Environment"

