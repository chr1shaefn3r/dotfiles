#!/usr/bin/env bash

if [ -d ~/Movies/ ]; then
	exit 0;
fi

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

mkdir -p ~/Movies/backup/
mkdir -p ~/Movies/codieren/
m_success "Created ~/Movies directory structure"

cp ~/.dotfiles/activators/systemConfigs/buildMakeMkv.sh ~/Movies;
cp ~/.dotfiles/activators/systemConfigs/dvdezentrale-sicherungskopie ~/Movies;
m_success "Copied scripts over to Movies"
cp ~/.dotfiles/activators/systemConfigs/h265.sh ~/Movies/codieren/;
cp ~/.dotfiles/activators/systemConfigs/h265_vaapi.sh ~/Movies/codieren/;
m_success "Copied h265(_vaapi).sh over to Movies/codieren"

sudo apt-get install -y handbrake ffmpeg mkvtoolnix-gui
m_success "Installed handbrake, ffmpeg and mkvtoolnix-gui"

sudo apt-get install -y make gcc g++ zlib1g-dev libssl-dev libexpat1-dev libavutil-dev libavcodec-dev qt5-default
bash ~/Movies/buildMakeMkv.sh
m_success "Installed makemkv"
brave-browser "http://www.makemkv.com/forum2/viewtopic.php?f=5&t=1053"
m_success "Started brave to point at makemkv forum"

m_success "Successfully installed Movies Environment"
