#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install 3D-printing Environment"

CURA="Ultimaker_Cura-4.6.1.AppImage"
wget -O ~/Downloads/$CURA "https://software.ultimaker.com/cura/$CURA"
m_success "Downloaded $CURA"

chmod +x ~/Downloads/$CURA
sudo cp ~/Downloads/$CURA /opt/
ln -s /opt/$CURA ~/bin/cura
m_success "Installed $CURA"

sudo add-apt-repository -y ppa:freecad-maintainers/freecad-stable
sudo add-apt-repository -y ppa:openscad/releases
sudo apt-get update
sudo apt-get install -y freecad openscad
m_success "Installed FreeCAD and OpenSCAD"

m_success "Successfully installed 3D-printing Environment"

