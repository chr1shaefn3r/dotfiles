#!/usr/bin/env bash

if [ -d ~/Development/js/ ]; then
	exit 0;
fi

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

mkdir -p ~/Development/js;
m_success "Created ~/Development/js"

m_header "Install Nodejs Development Environment"
NODEREPO="node_12.x"
DISTRO=$(lsb_release -c -s)
wget -qO- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
echo "deb https://deb.nodesource.com/${NODEREPO} ${DISTRO} main" | sudo tee /etc/apt/sources.list.d/nodesource.list
echo "deb-src https://deb.nodesource.com/${NODEREPO} ${DISTRO} main" | sudo tee -a /etc/apt/sources.list.d/nodesource.list
sudo apt-get update
sudo apt-get install -y nodejs
m_success "Installed $NODEREPO"

(cd /tmp/ && sudo npm install -g npm)
m_success "Updated npm to the latest version"

(cd /tmp/ && sudo npm install -g yarn)
m_success "Successfully installed yarn"

(cd ~/Downloads/ && wget "https://go.microsoft.com/fwlink/?LinkID=760868" && sudo dpkg -i code*.deb)
m_success "Successfully installed Visual Studio Code"

m_success "Successfully installed JavaScript Development Environment"

