#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install Jekyll Environment"

m_header "Install ruby"
sudo apt-get update
sudo apt-get install -y ruby ruby-dev g++ make gcc
m_success "Installed ruby"

sudo apt-get install -y zlib1g-dev
m_success "Installed html-proofer dependencies"
sudo gem install html-proofer jekyll bundler
m_success "Installed html-proofer, jekyll and bundler"

m_success "Successfully installed Jekyll Environment"

