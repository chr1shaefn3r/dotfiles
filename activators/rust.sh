#!/usr/bin/env bash

set -e

if [ -d ~/Development/rust/ ]; then
	exit 0;
fi

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install rust development Environment"

mkdir -p ~/Development/rust;
m_success "Created ~/Development/rust"

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
m_success "Installed rust and cargo via rustup"

m_header "Hyperfine"
cargo install hyperfine

m_header "Run hello world"

cd ~/Development/rust
git clone git@gitlab.com:chr1shaefn3r-rust/hello_world.git
cd hello_world
cargo run

m_success "Successfully installed rust development Environment"

