#!/usr/bin/env bash

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install Obsidian"

# Version from: https://obsidian.md/download
OBSIDIAN_VERSION="0.12.15"
DEB_FILE="obsidian_${OBSIDIAN_VERSION}_amd64.deb"
wget -O "/tmp/$DEB_FILE" "https://github.com/obsidianmd/obsidian-releases/releases/download/v$OBSIDIAN_VERSION/$DEB_FILE"
m_success "Downloaded Obsidian v$OBSIDIAN_VERSION"

sudo dpkg -i "/tmp/$DEB_FILE"
m_success "Installed Obsidian v$OBSIDIAN_VERSION"

m_success "Successfully installed Obsidian"

