#!/usr/bin/env bash

set -e

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install Openshot Environment"

m_header "Add PPA"
sudo add-apt-repository -y ppa:openshot.developers/ppa
sudo apt-get update
m_success "Added Openshot PPA"

m_header "Install Openshot"
sudo apt-get -y install openshot-qt python3-openshot
m_success "Installed Openshot"

m_success "Successfully installed Openshot Environment"

