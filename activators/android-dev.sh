#!/usr/bin/env bash

DIR=$HOME"/Development/android"

if [ -d "$DIR" ]; then
	exit 0;
fi

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install Android Development Environment"
sudo apt-add-repository -y ppa:paolorotolo/android-studio
m_success "Added paolorotolo/android-studio"
sudo apt-get update
sudo apt-get install -y android-studio default-jdk
m_success "Installed android-studio"

mkdir -p $DIR;
m_success "Created $DIR"

########### Download sdk-wbpage ##################################### Grep line with android-sdk-link #### remove front ##################### remove back ################
ANDLINK=`wget -O - http://developer.android.com/sdk/index.html 2>&1| grep -m 1 "android-sdk.*-linux.tgz" | sed -e "s/.*href=\"//;s/ (.*//" | sed -e "s/\">.*//;s/ (.*//"`;

## Download and untar (hope it's gzipped)
cd $DIR;
wget -O - $ANDLINK | tar -xz;
m_success "Downloaded android-sdk-linux"

m_arrow "Proceed to install needed 32-bit libs"
sudo apt-get -y install lib32stdc++6 lib32z1 lib32z1-dev

m_arrow "Create symlink ~/bin/aandroid-studio.sh -> /opt/android-studio/bin/studio.sh"
ln -s /opt/android-studio/bin/studio.sh ~/bin/aandroid-studio

sudo apt-get -y install optipng
m_success "Installed 'optipng'"

m_success "Successfully installed Android Development Environment"

