#!/usr/bin/env bash

# Logging stuff.
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }

(cd ~/Downloads/ && curl -O https://prerelease.keybase.io/keybase_amd64.deb && sudo dpkg -i keybase_amd64.deb)
# if you see an error about missing `libappindicator1`
# from the next command, you can ignore it, as the
# subsequent command corrects it
sudo apt-get install -y -f
run_keybase

m_success "Successfully installed Keybase"

