#!/usr/bin/env bash

set -e

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

# Based on:
# https://grapheneos.org/install/cli

m_header "Switch to a tmp-dir"
TEMP_DIR=`mktemp --directory`
m_success "Created $TEMP_DIR"
cd "$TEMP_DIR"
m_success "Switched to $TEMP_DIR"

m_header "Install Fastboot tool"
sudo apt install -y libarchive-tools
curl -O https://dl.google.com/android/repository/platform-tools_r31.0.3-linux.zip
echo 'e6cb61b92b5669ed6fd9645fad836d8f888321cd3098b75588a54679c204b7dc  platform-tools_r31.0.3-linux.zip' | sha256sum -c
bsdtar xvf platform-tools_r31.0.3-linux.zip
export PATH="$PWD/platform-tools:$PATH"
m_success "Installed Fastboot tool"
fastboot --version

if ! [ $($(which fastboot) --version | grep "version" | cut -c18-23 | sed 's/\.//g' ) -ge 2802 ]; then
  m_error "fastboot too old; please download the latest version at https://developer.android.com/studio/releases/platform-tools.html"
  exit 1
fi

m_header "Install udev rules"
sudo apt install -y android-sdk-platform-tools-common

DEVICE_NAME="sunfish" # Pixel 4a (not 5g)
FACTORY_IMAGE="$DEVICE_NAME-factory-2021090819"

m_header "Download, verify and extract factory images"
sudo apt install -y signify-openbsd
curl -O https://releases.grapheneos.org/factory.pub
curl -O https://releases.grapheneos.org/$FACTORY_IMAGE.zip
curl -O https://releases.grapheneos.org/$FACTORY_IMAGE.zip.sig
signify-openbsd -Cqp factory.pub -x $FACTORY_IMAGE.zip.sig && m_success verified
bsdtar xvf $FACTORY_IMAGE.zip

m_header "Waiting for phone to be connected"
read -p "Press any key to continue... " -n1 -s

m_header "Unlock bootloader"
fastboot flashing unlock

m_header "Waiting bootloader unlock to be confirmed"
read -p "Press any key to continue... " -n1 -s

m_header "Flashing the factory image"
cd $FACTORY_IMAGE/
./flash-all.sh

m_header "Unlock bootloader"
read -p "Press any key to continue... " -n1 -s
fastboot flashing lock

