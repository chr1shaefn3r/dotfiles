#!/usr/bin/env bash

set -e

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Install OBS/Virtual Webcam"

m_header "Install prerequisite ffmpeg and v4l2loopback"
sudo apt-get install -y ffmpeg v4l2loopback-dkms
m_header "Install obsstudio"
sudo add-apt-repository -y ppa:obsproject/obs-studio
sudo apt-get install -y obs-studio

m_header "Activate v4l2loopback kernel module"
sudo modprobe v4l2loopback

m_success "Successfully installed OBS/Virtual Webcam"
m_success "Start obs studio -> Tools -> v4l2sink"

