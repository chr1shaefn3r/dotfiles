#!/usr/bin/env sh

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

# Exit after first error
set -e

sudo add-apt-repository -y universe
wget -O- https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
################################################################################
# Hook 'add-repositories'
################################################################################
################################################################################
sudo apt-get update
sudo apt-get install -y git git-svn vim screen sl htop bash-completion suckless-tools mmv tree net-tools \
	exfat-fuse \
	vlc \
	scrot \
	gparted \
	nextcloud-desktop \
	gimp \
	chromium-browser \
	pmount \
	texlive-full \
	sshfs \
	apt-transport-https \
	lsb-release \
	curl \
	mediainfo \
	keepass2 \
	filezilla \
	imagemagick \
	silversearcher-ag \
	libreoffice \
	gnupg2 \
	gnupg-agent \
	scdaemon \
	pcscd \
	docker.io \
	docker-compose \
	brave-browser

################################################################################
# Hook 'add-packages'
################################################################################
################################################################################

cd ~;

HOSTNAME=`hostname -s` # on macOS just hostname returns "hostname.local"
REPO="https://gitlab.com/chr1shaefn3r/dotfiles.git"
CLONE_DESTINATION=".dotfiles"

m_header "Environment"
m_arrow "Working dir: `pwd`"
m_arrow "hostname: $HOSTNAME"

if [ ! -d ".dotfiles" ]; then
	m_header "Clone git repository 'dotfiles' with branch '$HOSTNAME' to `pwd`/$CLONE_DESTINATION"
	git clone -b $HOSTNAME --single-branch $REPO $CLONE_DESTINATION
	if [ $? -ne 0 ]; then
		m_error "Failed to clone branch '$HOSTNAME'"
		m_arrow "Going for master ..."
		m_header "Clone git repository 'dotfiles' to `pwd`/$CLONE_DESTINATION"
		git clone $REPO $CLONE_DESTINATION
		if [ $? -ne 0 ]; then
			m_error "Could not clone git-repo 'dotfiles'"
			m_arrow "Make sure ssh-key setup is working!"
			exit 1;
		fi
	fi
else
	m_header "Update git repository ~/.dotfiles"
	(cd .dotfiles/ && git fetch origin && git reset --hard FETCH_HEAD && git clean -df)
fi

m_header "Starting bootstrap.sh"
(cd .dotfiles/ && source bootstrap.sh)

