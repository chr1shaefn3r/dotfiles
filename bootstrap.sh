#!/usr/bin/env sh

echo "  ______   _______ _________ _______ _________ _        _______  _______ "
echo " (  __  \\ (  ___  )\\__   __/(  ____ \\\\__   __/( \\      (  ____ \\(  ____ \\"
echo " | (  \\  )| (   ) |   ) (   | (    \\/   ) (   | (      | (    \\/| (    \\/"
echo " | |   ) || |   | |   | |   | (__       | |   | |      | (__    | (_____ "
echo " | |   | || |   | |   | |   |  __)      | |   | |      |  __)   (_____  )"
echo " | |   ) || |   | |   | |   | (         | |   | |      | (            ) |"
echo " | (__/  )| (___) |   | |   | )      ___) (___| (____/\\| (____/\\/\\____) |"
echo " (______/ (_______)   )_(   |/       \\_______/(_______/(_______/\\_______)"

# Logging stuff.
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }

m_header "Start copying ..."
rsync -avh --no-perms config/ ~;
m_header "Activate bash_profile"
source ~/.bashrc;

PROJECTSDIRECTORY="$HOME/Projects/"
if [ ! -d "$PROJECTSDIRECTORY" ]; then
	bash ~/.dotfiles/systemSetup.sh
else
	m_error "$PROJECTSDIRECTORY exists already, skipping systemSetup.sh"
fi

