#!/usr/bin/env sh

# Logging stuff
function m_header() { echo -e "\n\033[1m$@\033[0m"; }
function m_success() { echo -e " \033[1;32m✔\033[0m $@"; }
function m_error() { echo -e " \033[1;31m✖\033[0m $@"; }
function m_arrow() { echo -e " \033[1;33m➜\033[0m $@"; }



m_header "Setting up the system"
PROJECTSDIRECTORY="$HOME/Projects/"
mkdir -p $PROJECTSDIRECTORY
m_success "Added $PROJECTSDIRECTORY"

DEVDIRECTORY="$HOME/Development/"
mkdir -p $DEVDIRECTORY
m_success "Added $DEVDIRECTORY"

usermod -a -G docker $USER
m_success "Added $USER to docker group"

SSH_KEY="$HOME/.ssh/id_rsa"
if [ ! -f "$SSH_KEY" ]; then
	ssh-keygen -b 4096 -f "$SSH_KEY" -N ""
	m_success "Generated ssh-key to $SSH_KEY"
else
	m_success "ssh-key $SSH_KEY already exists"
fi

################################################################################
# Hook 'Post-systemSetup'
################################################################################
################################################################################

